Prometheus postgres_exporter
============================

.. currentmodule:: pglift.prometheus

Module :mod:`pglift.prometheus` exposes the following API functions for
monitoring management using `Prometheus postgres_exporter`_:

.. autofunction:: setup
.. autofunction:: drop
.. autofunction:: apply
.. autofunction:: exists
.. autofunction:: port
.. autofunction:: start
.. autofunction:: stop

.. _`Prometheus postgres_exporter`: https://github.com/prometheus-community/postgres_exporter
