Operations
==========

This section describes day-to-day operations using pglift through available
interfaces (command-line, Ansible, Python).

.. toctree::
    :maxdepth: 2

    instance
    roles
    databases
    privileges
    backup
    monitoring
