API
===

This section documents general aspects of the Python API that are not related
to a specific operation context but may apply to several of them. It also
describes the data models used through the API.

.. toctree::
    :maxdepth: 1
    :caption: Contents:

    datamodel
    exceptions
    ctx
    cmd
    conf
    instances
    databases
    roles
    privileges
    pgbackrest
    postgres_exporter
