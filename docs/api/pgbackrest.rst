pgbackrest
==========

Module :mod:`pglift.pgbackrest` exposes the following API functions for backup
management using pgBackRest_:

.. currentmodule:: pglift.pgbackrest

.. autofunction:: backup
.. autofunction:: expire
.. autofunction:: iter_backups
.. autofunction:: restore
.. autofunction:: backup_command
.. autofunction:: expire_command
.. autoclass:: BackupType
    :members:

.. _pgBackRest: https://pgbackrest.org/
