Instances
=========

.. currentmodule:: pglift.instance

Module :mod:`pglift.instance` exposes the following API to manipulate
instances:

.. autofunction:: apply
.. autofunction:: describe
.. autofunction:: exists
.. autofunction:: list
.. autofunction:: drop
.. autofunction:: init
.. autofunction:: configure
.. autofunction:: start
.. autofunction:: stop
.. autofunction:: restart
.. autofunction:: reload
.. autofunction:: status
.. autofunction:: check_status
.. autofunction:: running
.. autofunction:: stopped
.. autofunction:: promote
.. autofunction:: upgrade
.. autofunction:: settings
.. autofunction:: logs
