====================
pglift documentation
====================

pglift provides a command-line interface, a Python library and an Ansible
collection to deploy and manage production-ready instances of PostgreSQL.

Tutorials
=========

.. toctree::
    :maxdepth: 1

    tutorials/install
    tutorials/cli
    tutorials/ansible


User guides
===========

.. toctree::
    :maxdepth: 1

    user/setup/index
    user/ops/index

Reference
=========

.. toctree::
    :maxdepth: 1

    api/index

How to guides
=============

.. toctree::
    :maxdepth: 1

    howto/instance-shell
    howto/instance-env
    howto/instance-logs
    howto/database-backup-restore
    howto/database-maintenance
    howto/standby-setup
    dev


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
